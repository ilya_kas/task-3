package by.epamtc.jwd.all.task1.util;

import by.epamtc.jwd.all.task1.entity.Array;

import java.util.*;

public class Filter {
    private LinkedList<Integer> fib = new LinkedList<>();

    public Filter(){
        fib.add(1);
        fib.add(2);
    }

    public int[] getPrime(Array array){
        if (array == null)
            return new int[0];

        int[] values = array.getValues();
        int[] primeNumbers = new int[values.length]; //можно было через списки, но решил так
        int pointer = 0;
        for (int x : values) {
            if (isPrime(x))
                primeNumbers[pointer++] = x;
        }
        return Arrays.copyOf(primeNumbers, pointer);
    }

    private boolean isPrime(int x) {
        if (x<=0) return false;
        for (int i = 2; i < Math.sqrt(x); i++) {
            if (x % i == 0)
                return false;
        }
        return true;
    }

    public int[] getFibonacci(Array array){
        if (array == null)
            return new int[0];

        int[] values = array.getValues();
        int[] uniqueNumbers = new int[values.length];
        int pointer = 0;
        for (int x : values)
            if (isFibonacci(x))
                uniqueNumbers[pointer++] = x;

        return Arrays.copyOf(uniqueNumbers, pointer);
    }

    private boolean isFibonacci(int num) {
        while (fib.getLast()<num)
            fib.add(fib.get(fib.size()-2) + fib.get(fib.size()-1));

        return fib.contains(num);
    }

    public int[] get3digitNums(Array array){
        if (array == null)
            return new int[0];

        int[] values = array.getValues();
        int[] uniqueNumbers = new int[values.length];
        int pointer = 0;

        String strValue;
        Set<Character> set = new HashSet<>();
        for (int value : values) {
            strValue = String.valueOf(value);
            set.clear();
            for (char c:strValue.toCharArray())
                set.add(c);

            if (strValue.length() == 3 && strValue.length() == set.size())
                uniqueNumbers[pointer++] = Integer.parseInt(strValue);
        }
        return Arrays.copyOf(uniqueNumbers, pointer);
    }
}
