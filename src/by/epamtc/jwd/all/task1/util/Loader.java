package by.epamtc.jwd.all.task1.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class Loader {
    public int[] fromConsole(int n){
        Communicator in = new Communicator();

        if (n<0) return new int[0];
        int[] ans = new int[n];
        for (int i=0;i<n;i++)
            ans[i] = in.nextInt();
        return ans;
    }

    public int[] fromFile(File file){
        if (file==null || !file.canRead()) return new int[0];

        int[] ans = new int[0];
        try {
            Scanner scan = new Scanner(file);
            int n = scan.nextInt();
            ans = new int[n];
            for (int i=0;i<n;i++)
                ans[i] = scan.nextInt();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return ans;
    }

    public int[] fillRandom(int n){
        if (n<0) return new int[0];
        Random random = new Random();

        int[] ans = new int[n];
        for (int i=0;i<n;i++)
            ans[i] = random.nextInt();
        return ans;
    }
}
