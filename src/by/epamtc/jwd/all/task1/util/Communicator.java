package by.epamtc.jwd.all.task1.util;

import java.util.Scanner;

public class Communicator {

    Scanner scan = new Scanner(System.in);

    public int nextInt(){
        while (!scan.hasNextInt()) {
            scan.nextInt();
            System.out.println("Enter a number");
        }
        int num = scan.nextInt();
        return num;
    }

    public String nextLine(){
        while (!scan.hasNextLine());
        String line = scan.nextLine();
        return line;
    }
}
