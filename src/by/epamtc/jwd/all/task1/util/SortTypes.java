package by.epamtc.jwd.all.task1.util;

public enum SortTypes {
    BUBBLE_SORT, GNOME_SORT, MERGE_SORT;
}
