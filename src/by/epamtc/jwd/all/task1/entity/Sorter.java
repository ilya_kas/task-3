package by.epamtc.jwd.all.task1.entity;

import by.epamtc.jwd.all.task1.util.SortTypes;

import java.util.Arrays;

public class Sorter {
    public static void sort(Array a, SortTypes type){
        if (a==null) return;

        int[] array = a.getValues();
        switch (type){
            case BUBBLE_SORT: BubbleSort(array); break;
            case GNOME_SORT: GnomeSort(array); break;
            case MERGE_SORT: MergeSort(array,0,array.length-1); break;
        }
        a.setValues(array);

        a.setSorted(true);
    }

    private static void BubbleSort(int[] array){
        for (int i=0; i<array.length; i++)
            for (int j=i; j<array.length; j++)
                if (array[i]>array[j]){
                    int z = array[i];
                    array[i] = array[j];
                    array[j] = z;
                }
    }

    private static void GnomeSort(int[] array){
        for (int i=1; i<array.length; i++) {
            int j = i;
            while (0<j && array[j-1]>array[j]){
                int z = array[j];
                array[j] = array[j-1];
                array[j-1] = z;
                j--;
            }
        }
    }

    private static void MergeSort(int[] a, int l, int r) {
        if (r <= l)
            return;
        int mid = (l+r) / 2;
        MergeSort(a, l, mid);
        MergeSort(a, mid + 1, r);

        int[] buf = Arrays.copyOf(a, a.length);

        int i = l, j = mid + 1;
        for (int u = l; u <= r; u++) {
            if (i > mid) {
                a[u] = buf[j];
                j++;
            } else if (j > r) {
                a[u] = buf[i];
                i++;
            } else if (buf[j] < buf[i]) {
                a[u] = buf[j];
                j++;
            } else {
                a[u] = buf[i];
                i++;
            }
        }
    }
}
