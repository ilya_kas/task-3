package by.epamtc.jwd.all.task1.entity;

import by.epamtc.jwd.all.task1.exception.ArrayInitException;
import by.epamtc.jwd.all.task1.exception.EmptyArrayException;
import by.epamtc.jwd.all.task1.exception.IndexOutOfBoundsException;
import by.epamtc.jwd.all.task1.util.SortTypes;

import java.util.Arrays;

public class Array {
    private final int DEFAULT_SIZE = 100;
    private final int NOT_FOUND = 100;

    private int[] values;
    private boolean sorted = false;

    /**
     * конструкторы
     */
    public Array(){
        values = new int[DEFAULT_SIZE];
    }

    public Array(int count) throws ArrayInitException {
        if (count<=0)
            throw new ArrayInitException();
        values = new int[count];
    }

    public Array(int[] _values){
        if (_values == null) return;
        values = Arrays.copyOf(_values,_values.length);
    }

    /**
     * геттеры и сеттеры
     */
    public int[] getValues() {
        return values;
    }

    public int getValue(int nom) throws IndexOutOfBoundsException {
        if (values== null || 0>nom || nom>=values.length)
            throw new IndexOutOfBoundsException();
        return values[nom];
    }

    public int length(){
        if (values==null) return 0;
        return values.length;
    }

    public void setValues(int[] values) {
        if (values==null)
            return;
        this.values = values;
        sorted = false;
    }

    public void setValue(int nom, int value) throws IndexOutOfBoundsException, EmptyArrayException {
        if (values== null)
            throw new EmptyArrayException();
        if (0>nom || nom>=values.length)
            throw new IndexOutOfBoundsException();
        values[nom] = value;
        sorted = false;
    }

    public void setSorted(boolean sorted) {
        this.sorted = sorted;
    }

    /**
     * переопределение от Object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Array array = (Array) o;
        return Arrays.equals(values, array.values);
    }

    @Override
    public int hashCode() {
        if (values==null)
            return -1;

        int result = 0;
        for (int element : values)
            result = 2 * result ^ element;

        return result;
    }

    @Override
    public String toString() {
        return "Array{" + "values=" + Arrays.toString(values) + '}';
    }

    /**
     * задания
     */

    public int find(int x) throws EmptyArrayException {
        if (values== null)
            throw new EmptyArrayException();

        if (!sorted)
            Sorter.sort(this, SortTypes.MERGE_SORT);

        int l = 0;
        int r = values.length-1;
        while (l<r){
            int mid = (l+r)/2;
            if (values[mid]<x)
                l = mid+1;
            else
                r = mid;
        }
        if (l!=r) return NOT_FOUND;
        if (values[l]!=x) return NOT_FOUND;
        return l;
    }

    public int max() throws EmptyArrayException {
        if (values== null)
            throw new EmptyArrayException();

        int ans = 0;
        for (int i=1;i<values.length;i++)
            if (values[i]>values[ans])
                ans = i;
        return values[ans];
    }

    public int min() throws EmptyArrayException {
        if (values== null)
            throw new EmptyArrayException();

        int ans = 0;
        for (int i=1;i<values.length;i++)
            if (values[i]<values[ans])
                ans = i;
        return values[ans];
    }
}
