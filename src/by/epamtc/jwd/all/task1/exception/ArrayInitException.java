package by.epamtc.jwd.all.task1.exception;

public class ArrayInitException extends Exception{
    public ArrayInitException() {
        super();
    }

    public ArrayInitException(String message) {
        super(message);
    }

    public ArrayInitException(String message, Throwable cause) {
        super(message, cause);
    }

    public ArrayInitException(Throwable cause) {
        super(cause);
    }
}
