package by.epamtc.jwd.all.task1;

import by.epamtc.jwd.all.task1.entity.Array;
import by.epamtc.jwd.all.task1.entity.Sorter;
import by.epamtc.jwd.all.task1.exception.ArrayInitException;
import by.epamtc.jwd.all.task1.exception.EmptyArrayException;
import by.epamtc.jwd.all.task1.exception.IndexOutOfBoundsException;
import by.epamtc.jwd.all.task1.util.Communicator;
import by.epamtc.jwd.all.task1.util.Filter;
import by.epamtc.jwd.all.task1.util.SortTypes;

import java.util.Arrays;

public class Front {
    public static void main(String[] _args) {
        String command;
        System.out.println("Enter a command:");

        Array array = null;
        Filter filter = new Filter();
        int arg;
        int[] res;

        Communicator in = new Communicator();
        do {
            command = in.nextLine();

            //без аргументов
            switch (command){
                case "print":
                    if (array!= null)
                        System.out.println(array);
                    else
                        log("Array is empty");
                    continue;

                case "stop": continue;

                case "sort":
                    if (array!=null)
                        Sorter.sort(array, SortTypes.MERGE_SORT);
                    else
                        log("Array is empty");
                    continue;

                case "min":
                    if (array == null) {
                        log("Array is empty");
                        continue;
                    }
                    try {
                        System.out.println(array.min());
                    } catch (EmptyArrayException e) {
                        log("Array is empty");
                    }
                    continue;

                case "max":
                    if (array == null) {
                        log("Array is empty");
                        continue;
                    }
                    try {
                        System.out.println(array.max());
                    } catch (EmptyArrayException e) {
                        log("Array is empty");
                    }
                    continue;

                case "get prime":
                    if (array == null) {
                        log("Array is empty");
                        continue;
                    }
                    res = filter.getPrime(array);
                    System.out.println(Arrays.toString(res));
                    continue;

                case "get fib":
                    if (array == null) {
                        log("Array is empty");
                        continue;
                    }
                    res = filter.getFibonacci(array);
                    System.out.println(Arrays.toString(res));
                    continue;

                case "task 6":
                    if (array == null) {
                        log("Array is empty");
                        continue;
                    }
                    res = filter.get3digitNums(array);
                    System.out.println(Arrays.toString(res));
                    continue;
            }

            if (command.length()<4){
                log("Wrong command");
                continue;
            }

            switch (command.substring(0,4)){
                case "get ":
                    if (!command.substring(4).matches("\\d+")){
                        log("Аргумент должен быть числом");
                        continue;
                    }
                    arg = Integer.parseInt(command.substring(4));

                    if (array == null) {
                        log("Array is empty");
                        continue;
                    }
                    try {
                        int ans = array.getValue(arg);
                        System.out.println(ans);
                    } catch (IndexOutOfBoundsException e) {
                        log("Index out of bounds");
                    }
                    continue;

                case "set ":
                    if (!command.substring(4).matches("\\d+\\s-?\\d+")){
                        log("Arguments must be 2 numbers");
                        continue;
                    }

                    String[] args = command.split(" ");
                    int nom = Integer.parseInt(args[1]);
                    int value = Integer.parseInt(args[2]);

                    if (array == null) {
                        log("Array is empty");
                        continue;
                    }
                    try {
                        array.setValue(nom, value);
                    } catch (IndexOutOfBoundsException | EmptyArrayException e) {
                        log("Index out of bounds");
                    }
                    continue;

                case "new ":
                    if (!command.substring(4).matches("\\d+")){
                        log("Argument must be a number");
                        continue;
                    }
                    arg = Integer.parseInt(command.substring(4));
                    try {
                        array = new Array(arg);
                    } catch (ArrayInitException e) {
                        log("Wrong array length");
                    }
                    continue;
            }

            if (command.length()<5){
                log("Wrong command");
                continue;
            }

            if (command.startsWith("find ")) {
                if (array == null) {
                    log("Array is empty");
                    continue;
                }
                if (!command.substring(5).matches("-?\\d+")) {
                    log("Argument must be a number");
                    continue;
                }

                arg = Integer.parseInt(command.substring(5));
                try {
                    int pos = array.find(arg);
                    System.out.println(pos);
                } catch (EmptyArrayException e) {
                    log("Элемент не найден");
                }
                continue;
            }

            log("Wrong command");
        }while (!command.equals("stop"));
    }

    public static void log(String msg){
        System.out.println(msg);
    }
}
