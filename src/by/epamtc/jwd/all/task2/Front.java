package by.epamtc.jwd.all.task2;

import by.epamtc.jwd.all.task1.exception.IndexOutOfBoundsException;
import by.epamtc.jwd.all.task2.entity.JaggedArray;
import by.epamtc.jwd.all.task2.sort.SortTypes;

import java.util.Random;

public class Front {
    private static final int n = 10;
    private static final int MAX_LEN = 10;

    private static final Random random = new Random();

    public static void main(String[] args) {
        JaggedArray array = new JaggedArray(n);
        for (int i=0;i<n;i++)
            try {
                array.fillRandom(i, Math.abs(random.nextInt())%MAX_LEN + 1);
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Index out of bounds");
            }

        array.print();
        array.sort(SortTypes.MIN, false);
        array.print();
        array.sort(SortTypes.MAX, true);
        array.print();
        array.sort(SortTypes.SUM,false);
        array.print();
    }
}
