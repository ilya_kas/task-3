package by.epamtc.jwd.all.task2.sort;

public enum SortTypes {
    SUM, MIN, MAX;
}
