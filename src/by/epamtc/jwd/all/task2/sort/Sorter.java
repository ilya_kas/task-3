package by.epamtc.jwd.all.task2.sort;

public class Sorter {
    public static void sort(Pair[] array, boolean reversed){
        BubbleSort(array);
        if (reversed){
            for (int i=0; i<array.length/2; i++){
                Pair z = array[i];
                array[i] = array[array.length-i-1];
                array[array.length-i-1] = z;
            }
        }
    }

    private static void BubbleSort(Pair[] array){
        for (int i=0; i<array.length; i++)
            for (int j=i; j<array.length; j++)
                if (array[i].value>array[j].value){
                    Pair z = array[i];
                    array[i] = array[j];
                    array[j] = z;
                }
    }
}
