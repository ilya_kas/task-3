package by.epamtc.jwd.all.task2.entity;

import by.epamtc.jwd.all.task1.exception.IndexOutOfBoundsException;
import by.epamtc.jwd.all.task2.sort.Pair;
import by.epamtc.jwd.all.task2.sort.SortTypes;
import by.epamtc.jwd.all.task2.sort.Sorter;

import java.util.Arrays;
import java.util.Random;

public class JaggedArray {
    private static final int MAX_VALUE = 100;
    private static final Random random = new Random();

    private int[][] array;

    public JaggedArray(int n){
        array = new int[n][0];
    }

    public void fillRandom(int nom, int count) throws IndexOutOfBoundsException {
        if (0>nom || nom>=array.length)
            throw new IndexOutOfBoundsException();

        array[nom] = new int[count];
        for (int i=0; i<count; i++)
            array[nom][i] = random.nextInt() % MAX_VALUE;
    }

    public void setValues(int[] values, int nom){
        if (nom<0 || array.length<=nom) return;
        array[nom] = values;
    }

    public void sort(SortTypes type, boolean reversed){
        int n = array.length;
        Pair[] pairs = new Pair[n];
        for (int i=0; i<n;i++) {
            pairs[i] = new Pair();
            pairs[i].nom = i;
            switch (type){
                case MIN: pairs[i].value = min(array[i]);
                case MAX: pairs[i].value = max(array[i]);
                case SUM: pairs[i].value = sum(array[i]);
            }
        }
        Sorter.sort(pairs,reversed);

        int[][] tmp = new int[array.length][0];
        for (int i=0; i<array.length; i++)
            tmp[i] = array[pairs[i].nom];

        array = tmp;
    }

    private int min(int[] array){
        int min = 0;
        for (int i=1;i<array.length;i++)
            if (array[min]>array[i])
                min = i;
        return array[min];
    }

    private int max(int[] array){
        int max = 0;
        for (int i=1;i<array.length;i++)
            if (array[max]<array[i])
                max = i;
        return array[max];
    }

    private int sum(int[] array){
        int sum = 0;
        for (int j : array)
            sum += j;
        return sum;
    }

    public void print() {
        for (int i=0; i<array.length; i++){
            for (int j=0; j<array[i].length; j++)
                System.out.print(array[i][j]+" ");
            System.out.println();
        }
    }
}
